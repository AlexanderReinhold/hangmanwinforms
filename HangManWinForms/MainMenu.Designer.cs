﻿namespace HangManWinForms
{
    partial class MainMenu
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.SinglePlayer = new System.Windows.Forms.Button();
            this.MultiPlayer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SinglePlayer
            // 
            this.SinglePlayer.BackColor = System.Drawing.Color.Gray;
            this.SinglePlayer.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.SinglePlayer.FlatAppearance.BorderSize = 3;
            this.SinglePlayer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SinglePlayer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.SinglePlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SinglePlayer.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SinglePlayer.Location = new System.Drawing.Point(12, 200);
            this.SinglePlayer.Margin = new System.Windows.Forms.Padding(0);
            this.SinglePlayer.Name = "SinglePlayer";
            this.SinglePlayer.Size = new System.Drawing.Size(250, 50);
            this.SinglePlayer.TabIndex = 0;
            this.SinglePlayer.Text = "Single Player Game";
            this.SinglePlayer.UseVisualStyleBackColor = false;
            this.SinglePlayer.Click += new System.EventHandler(this.SinglePlayer_Click);
            // 
            // MultiPlayer
            // 
            this.MultiPlayer.BackColor = System.Drawing.Color.Gray;
            this.MultiPlayer.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.MultiPlayer.FlatAppearance.BorderSize = 3;
            this.MultiPlayer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MultiPlayer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.MultiPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiPlayer.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MultiPlayer.Location = new System.Drawing.Point(325, 200);
            this.MultiPlayer.Margin = new System.Windows.Forms.Padding(0);
            this.MultiPlayer.Name = "MultiPlayer";
            this.MultiPlayer.Size = new System.Drawing.Size(250, 50);
            this.MultiPlayer.TabIndex = 1;
            this.MultiPlayer.Text = "Multi Player Game";
            this.MultiPlayer.UseVisualStyleBackColor = false;
            this.MultiPlayer.Click += new System.EventHandler(this.MultiPlayer_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(584, 262);
            this.Controls.Add(this.MultiPlayer);
            this.Controls.Add(this.SinglePlayer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu";
            this.Text = "HangMan - Hauptmenü";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMenu_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SinglePlayer;
        private System.Windows.Forms.Button MultiPlayer;
    }
}