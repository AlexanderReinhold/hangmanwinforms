﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HangManWinForms
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void SinglePlayer_Click(object sender, EventArgs e)
        {
            SinglePlayer singlePlayer = new SinglePlayer();
            singlePlayer.Show();
            singlePlayer.StartSPGame();
            this.Hide();
        }

        private void MultiPlayer_Click(object sender, EventArgs e)
        {
            MultiPlayer multiPlayer = new MultiPlayer();
            multiPlayer.Show();
            multiPlayer.StartMPGame();
            this.Hide();
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
