﻿namespace HangManWinForms
{
    partial class MultiPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiPlayer));
            this.A = new System.Windows.Forms.Button();
            this.B = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Button();
            this.D = new System.Windows.Forms.Button();
            this.E = new System.Windows.Forms.Button();
            this.F = new System.Windows.Forms.Button();
            this.G = new System.Windows.Forms.Button();
            this.H = new System.Windows.Forms.Button();
            this.I = new System.Windows.Forms.Button();
            this.J = new System.Windows.Forms.Button();
            this.K = new System.Windows.Forms.Button();
            this.L = new System.Windows.Forms.Button();
            this.M = new System.Windows.Forms.Button();
            this.N = new System.Windows.Forms.Button();
            this.O = new System.Windows.Forms.Button();
            this.P = new System.Windows.Forms.Button();
            this.Q = new System.Windows.Forms.Button();
            this.R = new System.Windows.Forms.Button();
            this.S = new System.Windows.Forms.Button();
            this.T = new System.Windows.Forms.Button();
            this.U = new System.Windows.Forms.Button();
            this.V = new System.Windows.Forms.Button();
            this.W = new System.Windows.Forms.Button();
            this.X = new System.Windows.Forms.Button();
            this.Y = new System.Windows.Forms.Button();
            this.Z = new System.Windows.Forms.Button();
            this.Ä = new System.Windows.Forms.Button();
            this.Ö = new System.Windows.Forms.Button();
            this.Ü = new System.Windows.Forms.Button();
            this._ = new System.Windows.Forms.Button();
            this.Rate_Buchstaben = new System.Windows.Forms.GroupBox();
            this.wort = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.hangingman = new System.Windows.Forms.PictureBox();
            this.restart = new System.Windows.Forms.Button();
            this.mainmenu = new System.Windows.Forms.Button();
            this.Rate_Buchstaben.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hangingman)).BeginInit();
            this.SuspendLayout();
            // 
            // A
            // 
            this.A.BackColor = System.Drawing.Color.DimGray;
            this.A.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.A.FlatAppearance.BorderSize = 3;
            this.A.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.A.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.A.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A.Location = new System.Drawing.Point(9, 19);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(25, 25);
            this.A.TabIndex = 0;
            this.A.Text = "A";
            this.A.UseVisualStyleBackColor = false;
            this.A.Click += new System.EventHandler(this.Letter_Click);
            // 
            // B
            // 
            this.B.BackColor = System.Drawing.Color.DimGray;
            this.B.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.B.FlatAppearance.BorderSize = 3;
            this.B.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.B.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.B.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.B.Location = new System.Drawing.Point(40, 19);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(25, 25);
            this.B.TabIndex = 1;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = false;
            this.B.Click += new System.EventHandler(this.Letter_Click);
            // 
            // C
            // 
            this.C.BackColor = System.Drawing.Color.DimGray;
            this.C.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.C.FlatAppearance.BorderSize = 3;
            this.C.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.C.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.C.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.C.Location = new System.Drawing.Point(71, 19);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(25, 25);
            this.C.TabIndex = 2;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = false;
            this.C.Click += new System.EventHandler(this.Letter_Click);
            // 
            // D
            // 
            this.D.BackColor = System.Drawing.Color.DimGray;
            this.D.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.D.FlatAppearance.BorderSize = 3;
            this.D.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.D.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.D.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.D.Location = new System.Drawing.Point(102, 19);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(25, 25);
            this.D.TabIndex = 3;
            this.D.Text = "D";
            this.D.UseVisualStyleBackColor = false;
            this.D.Click += new System.EventHandler(this.Letter_Click);
            // 
            // E
            // 
            this.E.BackColor = System.Drawing.Color.DimGray;
            this.E.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.E.FlatAppearance.BorderSize = 3;
            this.E.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.E.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.E.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.E.Location = new System.Drawing.Point(133, 19);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(25, 25);
            this.E.TabIndex = 4;
            this.E.Text = "E";
            this.E.UseVisualStyleBackColor = false;
            this.E.Click += new System.EventHandler(this.Letter_Click);
            // 
            // F
            // 
            this.F.BackColor = System.Drawing.Color.DimGray;
            this.F.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.F.FlatAppearance.BorderSize = 3;
            this.F.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.F.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.F.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.F.Location = new System.Drawing.Point(164, 19);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(25, 25);
            this.F.TabIndex = 5;
            this.F.Text = "F";
            this.F.UseVisualStyleBackColor = false;
            this.F.Click += new System.EventHandler(this.Letter_Click);
            // 
            // G
            // 
            this.G.BackColor = System.Drawing.Color.DimGray;
            this.G.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.G.FlatAppearance.BorderSize = 3;
            this.G.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.G.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.G.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.G.Location = new System.Drawing.Point(195, 19);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(25, 25);
            this.G.TabIndex = 6;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = false;
            this.G.Click += new System.EventHandler(this.Letter_Click);
            // 
            // H
            // 
            this.H.BackColor = System.Drawing.Color.DimGray;
            this.H.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.H.FlatAppearance.BorderSize = 3;
            this.H.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.H.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.H.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.H.Location = new System.Drawing.Point(226, 19);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(25, 25);
            this.H.TabIndex = 7;
            this.H.Text = "H";
            this.H.UseVisualStyleBackColor = false;
            this.H.Click += new System.EventHandler(this.Letter_Click);
            // 
            // I
            // 
            this.I.BackColor = System.Drawing.Color.DimGray;
            this.I.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.I.FlatAppearance.BorderSize = 3;
            this.I.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.I.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.I.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.I.Location = new System.Drawing.Point(257, 19);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(25, 25);
            this.I.TabIndex = 8;
            this.I.Text = "I";
            this.I.UseVisualStyleBackColor = false;
            this.I.Click += new System.EventHandler(this.Letter_Click);
            // 
            // J
            // 
            this.J.BackColor = System.Drawing.Color.DimGray;
            this.J.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.J.FlatAppearance.BorderSize = 3;
            this.J.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.J.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.J.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.J.Location = new System.Drawing.Point(288, 19);
            this.J.Name = "J";
            this.J.Size = new System.Drawing.Size(25, 25);
            this.J.TabIndex = 9;
            this.J.Text = "J";
            this.J.UseVisualStyleBackColor = false;
            this.J.Click += new System.EventHandler(this.Letter_Click);
            // 
            // K
            // 
            this.K.BackColor = System.Drawing.Color.DimGray;
            this.K.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.K.FlatAppearance.BorderSize = 3;
            this.K.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.K.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.K.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.K.Location = new System.Drawing.Point(319, 19);
            this.K.Name = "K";
            this.K.Size = new System.Drawing.Size(25, 25);
            this.K.TabIndex = 10;
            this.K.Text = "K";
            this.K.UseVisualStyleBackColor = false;
            this.K.Click += new System.EventHandler(this.Letter_Click);
            // 
            // L
            // 
            this.L.BackColor = System.Drawing.Color.DimGray;
            this.L.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.L.FlatAppearance.BorderSize = 3;
            this.L.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.L.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.L.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.L.Location = new System.Drawing.Point(350, 19);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(25, 25);
            this.L.TabIndex = 11;
            this.L.Text = "L";
            this.L.UseVisualStyleBackColor = false;
            this.L.Click += new System.EventHandler(this.Letter_Click);
            // 
            // M
            // 
            this.M.BackColor = System.Drawing.Color.DimGray;
            this.M.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.M.FlatAppearance.BorderSize = 3;
            this.M.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.M.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.M.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.M.Location = new System.Drawing.Point(381, 19);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(25, 25);
            this.M.TabIndex = 12;
            this.M.Text = "M";
            this.M.UseVisualStyleBackColor = false;
            this.M.Click += new System.EventHandler(this.Letter_Click);
            // 
            // N
            // 
            this.N.BackColor = System.Drawing.Color.DimGray;
            this.N.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.N.FlatAppearance.BorderSize = 3;
            this.N.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.N.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.N.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.N.Location = new System.Drawing.Point(412, 19);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(25, 25);
            this.N.TabIndex = 13;
            this.N.Text = "N";
            this.N.UseVisualStyleBackColor = false;
            this.N.Click += new System.EventHandler(this.Letter_Click);
            // 
            // O
            // 
            this.O.BackColor = System.Drawing.Color.DimGray;
            this.O.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.O.FlatAppearance.BorderSize = 3;
            this.O.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.O.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.O.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.O.Location = new System.Drawing.Point(443, 19);
            this.O.Name = "O";
            this.O.Size = new System.Drawing.Size(25, 25);
            this.O.TabIndex = 14;
            this.O.Text = "O";
            this.O.UseVisualStyleBackColor = false;
            this.O.Click += new System.EventHandler(this.Letter_Click);
            // 
            // P
            // 
            this.P.BackColor = System.Drawing.Color.DimGray;
            this.P.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.P.FlatAppearance.BorderSize = 3;
            this.P.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.P.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.P.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.P.Location = new System.Drawing.Point(9, 50);
            this.P.Name = "P";
            this.P.Size = new System.Drawing.Size(25, 25);
            this.P.TabIndex = 15;
            this.P.Text = "P";
            this.P.UseVisualStyleBackColor = false;
            this.P.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Q
            // 
            this.Q.BackColor = System.Drawing.Color.DimGray;
            this.Q.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Q.FlatAppearance.BorderSize = 3;
            this.Q.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Q.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Q.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Q.Location = new System.Drawing.Point(40, 50);
            this.Q.Name = "Q";
            this.Q.Size = new System.Drawing.Size(25, 25);
            this.Q.TabIndex = 16;
            this.Q.Text = "Q";
            this.Q.UseVisualStyleBackColor = false;
            this.Q.Click += new System.EventHandler(this.Letter_Click);
            // 
            // R
            // 
            this.R.BackColor = System.Drawing.Color.DimGray;
            this.R.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.R.FlatAppearance.BorderSize = 3;
            this.R.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.R.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.R.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R.Location = new System.Drawing.Point(71, 50);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(25, 25);
            this.R.TabIndex = 17;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = false;
            this.R.Click += new System.EventHandler(this.Letter_Click);
            // 
            // S
            // 
            this.S.BackColor = System.Drawing.Color.DimGray;
            this.S.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.S.FlatAppearance.BorderSize = 3;
            this.S.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.S.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.S.Location = new System.Drawing.Point(102, 50);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(25, 25);
            this.S.TabIndex = 18;
            this.S.Text = "S";
            this.S.UseVisualStyleBackColor = false;
            this.S.Click += new System.EventHandler(this.Letter_Click);
            // 
            // T
            // 
            this.T.BackColor = System.Drawing.Color.DimGray;
            this.T.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.T.FlatAppearance.BorderSize = 3;
            this.T.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.T.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.T.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.T.Location = new System.Drawing.Point(133, 50);
            this.T.Name = "T";
            this.T.Size = new System.Drawing.Size(25, 25);
            this.T.TabIndex = 19;
            this.T.Text = "T";
            this.T.UseVisualStyleBackColor = false;
            this.T.Click += new System.EventHandler(this.Letter_Click);
            // 
            // U
            // 
            this.U.BackColor = System.Drawing.Color.DimGray;
            this.U.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.U.FlatAppearance.BorderSize = 3;
            this.U.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.U.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.U.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.U.Location = new System.Drawing.Point(164, 50);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(25, 25);
            this.U.TabIndex = 20;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = false;
            this.U.Click += new System.EventHandler(this.Letter_Click);
            // 
            // V
            // 
            this.V.BackColor = System.Drawing.Color.DimGray;
            this.V.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.V.FlatAppearance.BorderSize = 3;
            this.V.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.V.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.V.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.V.Location = new System.Drawing.Point(195, 50);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(25, 25);
            this.V.TabIndex = 21;
            this.V.Text = "V";
            this.V.UseVisualStyleBackColor = false;
            this.V.Click += new System.EventHandler(this.Letter_Click);
            // 
            // W
            // 
            this.W.BackColor = System.Drawing.Color.DimGray;
            this.W.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.W.FlatAppearance.BorderSize = 3;
            this.W.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.W.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.W.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.W.Location = new System.Drawing.Point(226, 50);
            this.W.Name = "W";
            this.W.Size = new System.Drawing.Size(25, 25);
            this.W.TabIndex = 22;
            this.W.Text = "W";
            this.W.UseVisualStyleBackColor = false;
            this.W.Click += new System.EventHandler(this.Letter_Click);
            // 
            // X
            // 
            this.X.BackColor = System.Drawing.Color.DimGray;
            this.X.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.X.FlatAppearance.BorderSize = 3;
            this.X.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.X.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.X.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.X.Location = new System.Drawing.Point(257, 50);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(25, 25);
            this.X.TabIndex = 23;
            this.X.Text = "X";
            this.X.UseVisualStyleBackColor = false;
            this.X.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Y
            // 
            this.Y.BackColor = System.Drawing.Color.DimGray;
            this.Y.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Y.FlatAppearance.BorderSize = 3;
            this.Y.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Y.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Y.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Y.Location = new System.Drawing.Point(288, 50);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(25, 25);
            this.Y.TabIndex = 24;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = false;
            this.Y.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Z
            // 
            this.Z.BackColor = System.Drawing.Color.DimGray;
            this.Z.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Z.FlatAppearance.BorderSize = 3;
            this.Z.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Z.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Z.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Z.Location = new System.Drawing.Point(319, 50);
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(25, 25);
            this.Z.TabIndex = 25;
            this.Z.Text = "Z";
            this.Z.UseVisualStyleBackColor = false;
            this.Z.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Ä
            // 
            this.Ä.BackColor = System.Drawing.Color.DimGray;
            this.Ä.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Ä.FlatAppearance.BorderSize = 3;
            this.Ä.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Ä.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Ä.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ä.Location = new System.Drawing.Point(350, 50);
            this.Ä.Name = "Ä";
            this.Ä.Size = new System.Drawing.Size(25, 25);
            this.Ä.TabIndex = 26;
            this.Ä.Text = "Ä";
            this.Ä.UseVisualStyleBackColor = false;
            this.Ä.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Ö
            // 
            this.Ö.BackColor = System.Drawing.Color.DimGray;
            this.Ö.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Ö.FlatAppearance.BorderSize = 3;
            this.Ö.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Ö.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Ö.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ö.Location = new System.Drawing.Point(381, 50);
            this.Ö.Name = "Ö";
            this.Ö.Size = new System.Drawing.Size(25, 25);
            this.Ö.TabIndex = 27;
            this.Ö.Text = "Ö";
            this.Ö.UseVisualStyleBackColor = false;
            this.Ö.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Ü
            // 
            this.Ü.BackColor = System.Drawing.Color.DimGray;
            this.Ü.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.Ü.FlatAppearance.BorderSize = 3;
            this.Ü.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.Ü.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.Ü.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ü.Location = new System.Drawing.Point(412, 50);
            this.Ü.Name = "Ü";
            this.Ü.Size = new System.Drawing.Size(25, 25);
            this.Ü.TabIndex = 28;
            this.Ü.Text = "Ü";
            this.Ü.UseVisualStyleBackColor = false;
            this.Ü.Click += new System.EventHandler(this.Letter_Click);
            // 
            // _
            // 
            this._.BackColor = System.Drawing.Color.DimGray;
            this._.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this._.FlatAppearance.BorderSize = 3;
            this._.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this._.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this._.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._.Location = new System.Drawing.Point(443, 50);
            this._.Name = "_";
            this._.Size = new System.Drawing.Size(25, 25);
            this._.TabIndex = 29;
            this._.Text = "/";
            this._.UseVisualStyleBackColor = false;
            this._.Click += new System.EventHandler(this.Letter_Click);
            // 
            // Rate_Buchstaben
            // 
            this.Rate_Buchstaben.Controls.Add(this.H);
            this.Rate_Buchstaben.Controls.Add(this._);
            this.Rate_Buchstaben.Controls.Add(this.A);
            this.Rate_Buchstaben.Controls.Add(this.Ü);
            this.Rate_Buchstaben.Controls.Add(this.B);
            this.Rate_Buchstaben.Controls.Add(this.Ö);
            this.Rate_Buchstaben.Controls.Add(this.C);
            this.Rate_Buchstaben.Controls.Add(this.Ä);
            this.Rate_Buchstaben.Controls.Add(this.D);
            this.Rate_Buchstaben.Controls.Add(this.Z);
            this.Rate_Buchstaben.Controls.Add(this.E);
            this.Rate_Buchstaben.Controls.Add(this.Y);
            this.Rate_Buchstaben.Controls.Add(this.F);
            this.Rate_Buchstaben.Controls.Add(this.X);
            this.Rate_Buchstaben.Controls.Add(this.G);
            this.Rate_Buchstaben.Controls.Add(this.W);
            this.Rate_Buchstaben.Controls.Add(this.I);
            this.Rate_Buchstaben.Controls.Add(this.V);
            this.Rate_Buchstaben.Controls.Add(this.J);
            this.Rate_Buchstaben.Controls.Add(this.U);
            this.Rate_Buchstaben.Controls.Add(this.K);
            this.Rate_Buchstaben.Controls.Add(this.T);
            this.Rate_Buchstaben.Controls.Add(this.L);
            this.Rate_Buchstaben.Controls.Add(this.S);
            this.Rate_Buchstaben.Controls.Add(this.M);
            this.Rate_Buchstaben.Controls.Add(this.R);
            this.Rate_Buchstaben.Controls.Add(this.N);
            this.Rate_Buchstaben.Controls.Add(this.Q);
            this.Rate_Buchstaben.Controls.Add(this.O);
            this.Rate_Buchstaben.Controls.Add(this.P);
            this.Rate_Buchstaben.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rate_Buchstaben.Location = new System.Drawing.Point(12, 356);
            this.Rate_Buchstaben.Name = "Rate_Buchstaben";
            this.Rate_Buchstaben.Size = new System.Drawing.Size(482, 86);
            this.Rate_Buchstaben.TabIndex = 30;
            this.Rate_Buchstaben.TabStop = false;
            this.Rate_Buchstaben.Text = "Rate Los!";
            // 
            // wort
            // 
            this.wort.BackColor = System.Drawing.Color.Gray;
            this.wort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wort.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wort.Location = new System.Drawing.Point(12, 318);
            this.wort.Name = "wort";
            this.wort.ReadOnly = true;
            this.wort.Size = new System.Drawing.Size(560, 23);
            this.wort.TabIndex = 30;
            this.wort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hangingman
            // 
            this.hangingman.Location = new System.Drawing.Point(12, 12);
            this.hangingman.Name = "hangingman";
            this.hangingman.Size = new System.Drawing.Size(300, 300);
            this.hangingman.TabIndex = 31;
            this.hangingman.TabStop = false;
            // 
            // restart
            // 
            this.restart.BackColor = System.Drawing.Color.DimGray;
            this.restart.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.restart.FlatAppearance.BorderSize = 3;
            this.restart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.restart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.restart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restart.Location = new System.Drawing.Point(332, 12);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(240, 52);
            this.restart.TabIndex = 33;
            this.restart.Text = "Neues Wort/Neustart";
            this.restart.UseVisualStyleBackColor = false;
            // 
            // mainmenu
            // 
            this.mainmenu.BackColor = System.Drawing.Color.DimGray;
            this.mainmenu.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.mainmenu.FlatAppearance.BorderSize = 3;
            this.mainmenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.mainmenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.mainmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainmenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainmenu.Location = new System.Drawing.Point(332, 70);
            this.mainmenu.Name = "mainmenu";
            this.mainmenu.Size = new System.Drawing.Size(240, 52);
            this.mainmenu.TabIndex = 34;
            this.mainmenu.Text = "Hauptmenü";
            this.mainmenu.UseVisualStyleBackColor = false;
            // 
            // MultiPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(584, 454);
            this.Controls.Add(this.mainmenu);
            this.Controls.Add(this.restart);
            this.Controls.Add(this.hangingman);
            this.Controls.Add(this.wort);
            this.Controls.Add(this.Rate_Buchstaben);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MultiPlayer";
            this.Text = "HangMan - Multi Player";
            this.Rate_Buchstaben.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hangingman)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button A;
        private System.Windows.Forms.Button B;
        private System.Windows.Forms.Button C;
        private System.Windows.Forms.Button D;
        private System.Windows.Forms.Button E;
        private System.Windows.Forms.Button F;
        private System.Windows.Forms.Button G;
        private System.Windows.Forms.Button H;
        private System.Windows.Forms.Button I;
        private System.Windows.Forms.Button J;
        private System.Windows.Forms.Button K;
        private System.Windows.Forms.Button L;
        private System.Windows.Forms.Button M;
        private System.Windows.Forms.Button N;
        private System.Windows.Forms.Button O;
        private System.Windows.Forms.Button P;
        private System.Windows.Forms.Button Q;
        private System.Windows.Forms.Button R;
        private System.Windows.Forms.Button S;
        private System.Windows.Forms.Button T;
        private System.Windows.Forms.Button U;
        private System.Windows.Forms.Button V;
        private System.Windows.Forms.Button W;
        private System.Windows.Forms.Button X;
        private System.Windows.Forms.Button Y;
        private System.Windows.Forms.Button Z;
        private System.Windows.Forms.Button Ä;
        private System.Windows.Forms.Button Ö;
        private System.Windows.Forms.Button Ü;
        private System.Windows.Forms.Button _;
        private System.Windows.Forms.GroupBox Rate_Buchstaben;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.TextBox wort;
        private System.Windows.Forms.PictureBox hangingman;
        private System.Windows.Forms.Button restart;
        private System.Windows.Forms.Button mainmenu;

    }
}