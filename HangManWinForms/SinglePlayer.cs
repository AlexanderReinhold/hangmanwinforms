﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace HangManWinForms
{
    public partial class SinglePlayer : Form
    {
        string[] words;

        List<Button> buttons = new List<Button>();

        bool running = true;
        int fails = 0;

        Bitmap hang_1 = new Bitmap("hangman\\1.png");
        Bitmap hang_2 = new Bitmap("hangman\\2.png");
        Bitmap hang_3 = new Bitmap("hangman\\3.png");
        Bitmap hang_4 = new Bitmap("hangman\\4.png");
        Bitmap hang_5 = new Bitmap("hangman\\5.png");
        Bitmap hang_6 = new Bitmap("hangman\\6.png");
        Bitmap hang_7 = new Bitmap("hangman\\7.png");
        Bitmap hang_8 = new Bitmap("hangman\\8.png");
        Bitmap hang_9 = new Bitmap("hangman\\9.png");
        Bitmap hang_10 = new Bitmap("hangman\\10.png");
        Bitmap hang_11 = new Bitmap("hangman\\11.png");
        Bitmap hang_win = new Bitmap("hangman\\win.png");

        string currentWord;
        char[] currentDis;

        public SinglePlayer()
        {
            InitializeComponent();

            buttons.Add(this.A);
            buttons.Add(this.B);
            buttons.Add(this.C);
            buttons.Add(this.D);
            buttons.Add(this.E);
            buttons.Add(this.F);
            buttons.Add(this.G);
            buttons.Add(this.H);
            buttons.Add(this.I);
            buttons.Add(this.J);
            buttons.Add(this.K);
            buttons.Add(this.L);
            buttons.Add(this.M);
            buttons.Add(this.N);
            buttons.Add(this.O);
            buttons.Add(this.P);
            buttons.Add(this.Q);
            buttons.Add(this.R);
            buttons.Add(this.S);
            buttons.Add(this.T);
            buttons.Add(this.U);
            buttons.Add(this.V);
            buttons.Add(this.W);
            buttons.Add(this.X);
            buttons.Add(this.Y);
            buttons.Add(this.Z);
            buttons.Add(this.Ä);
            buttons.Add(this.Ö);
            buttons.Add(this.Ü);

        }

        private void Letter_Click(object sender, EventArgs e)
        {

            if (running)
            {
                Button letter = (Button)sender;

                for (int i = 65; i <= 90; i++)
                {
                    string currLetter = "" + (char)i;
                    if (letter.Text == currLetter)
                    {
                        if (currentWord.Contains(currLetter))
                        {
                            UpdateWord(letter.Text[0]);
                        }
                        else
                        {
                            fails++;
                        }
                        letter.Visible = false;
                    }

                    UpdateGraphic();

                }

                switch (letter.Text)
                {
                    case "Ä":

                        if (currentWord.Contains(letter.Text))
                        {
                            UpdateWord(letter.Text[0]);
                        }
                        else
                        {
                            fails++;
                        }
                        letter.Visible = false;
                        UpdateGraphic();

                        break;
                    case "Ö":

                        if (currentWord.Contains(letter.Text))
                        {
                            UpdateWord(letter.Text[0]);
                        }
                        else
                        {
                            fails++;
                        }
                        letter.Visible = false;
                        UpdateGraphic();

                        break;
                    case "Ü":

                        if (currentWord.Contains(letter.Text))
                        {
                            UpdateWord(letter.Text[0]);
                        }
                        else
                        {
                            fails++;
                        }
                        letter.Visible = false;
                        UpdateGraphic();

                        break;
                }

                if (!currentDis.Contains('_'))
                {
                    running = false;
                    hangingman.Image = hang_win;
                }

            }
        }

        public void StartSPGame()
        {

            foreach (Button b in buttons)
            {
                b.Visible = true;
            }

            hangingman.Image = null;
            fails = 0;
            running = true;

            words = File.ReadAllLines("hangman\\words.txt");
            Random rand = new Random();
            currentWord = words[rand.Next(0, words.Length)].ToUpper();
            currentDis = new char[currentWord.Length * 2];

            for (int i = 0; i < (currentWord.Length * 2) - 1; i++)
            {
                currentDis[i] = '_';
                i++;
                currentDis[i] = ' ';
            }

            wort.Text = ArrayToString(currentDis);

        }

        public void UpdateGraphic()
        {

            switch (fails)
            {
                case 1:
                    hangingman.Image = hang_1;
                    break;

                case 2:
                    hangingman.Image = hang_2;
                    break;

                case 3:
                    hangingman.Image = hang_3;
                    break;

                case 4:
                    hangingman.Image = hang_4;
                    break;

                case 5:
                    hangingman.Image = hang_5;
                    break;

                case 6:
                    hangingman.Image = hang_6;
                    break;

                case 7:
                    hangingman.Image = hang_7;
                    break;

                case 8:
                    hangingman.Image = hang_8;
                    break;

                case 9:
                    hangingman.Image = hang_9;
                    break;

                case 10:
                    hangingman.Image = hang_10;
                    break;

                case 11:
                    hangingman.Image = hang_11;
                    running = false;
                    break;
            }

        }

        public void UpdateWord(char letter)
        {
            if (running == true)
            {
                int i = 0;
                foreach (char x in currentWord)
                {
                    if (x == letter)
                    {
                        currentDis[i] = x;
                    }

                    i += 2;

                }

                wort.Text = ArrayToString(currentDis);
            }

        }

        public string ArrayToString(char[] array)
        {
            string arraystring = "";
            foreach (char x in array)
            {
                arraystring = arraystring + x;
            }
            return arraystring;
        }

        private void restart_Click(object sender, EventArgs e)
        {
            StartSPGame();
        }

        private void mainmenu_Click(object sender, EventArgs e)
        {
            MainMenu hmh = new MainMenu();
            this.Close();
            hmh.Show();
        }

    }
}
